const register = document.querySelector('.register');
const login = document.querySelector('.login');
const logout = document.querySelector('.logout');
const registerSection = document.querySelector('.register-section');
const main = document.querySelector('.main');
const loginSection = document.querySelector('.login-section');
const home = document.querySelector('.home');
const loads = document.querySelector('.loads');
const profile = document.querySelector('.profile');
const trucks = document.querySelector('.trucks');
const loadsSection = document.querySelector('.loads-section');
const trucksSection = document.querySelector('.trucks-section');
const profileSection = document.querySelector('.profile-section');
const content = document.querySelector('.content');
const backdrop = document.querySelector('.backdrop');
const closeBtn = document.querySelector('.button-close');
const closeTruckBtn = document.querySelector('.button-close-truck');
const loadForm = document.querySelector('.form-load');
const backdropDimensions = document.querySelector('.backdrop-dimensions');
const backdropTruck = document.querySelector('.backdrop-truck');
const truckForm = document.querySelector('.form-truck');

let success = true;

let registerForm;
let loginForm;
let forgotPasswordLink;
let forgotPasswordLinkWrapper;
let loadsList;
let truckList;
let newLoads;
let assignedLoads;
let history;
let addLoad;
let postBtns;
let assignBtns;
let deleteBtns;
let editBtns;
let method;
let url;
let activeLoadLink;
let loadData;
const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};
let userInfo = {};
let userRole = localStorage.getItem('role');
const token = localStorage.getItem('token') ?? null;

if (!token) {
  loads.hidden = true;
  trucks.hidden = true;
  logout.hidden = true;
  profile.hidden = true;
}

if (token) {
  register.hidden = true;
  login.hidden = true;
}

if (userRole === 'SHIPPER') {
  trucks.hidden = true;
}

register.addEventListener('click', onRegisterBtnClick);
login.addEventListener('click', onLoginBtnClick);
logout.addEventListener('click', onLogoutBtnClick);

// ---------------------Register-------------------------

function onRegisterBtnClick() {
  loginSection.innerHTML = '';
  registerSection.innerHTML = `<form action="http://localhost:8080/api/auth/register" class="register-form" method="post">
    <legend class="form-title">Register</legend>
    <label for="email" class="form-label">Email</label>
    <input id="email" type="email" name="email" class="form-input">
    <label for="password" class="form-label">Password</label>
    <input id="password" type="password" name="password" class="form-input">
    <label for="select" class="form-label">Role
    <select id="select" name="role" class="form-select">
      <option value="SHIPPER" class="form-option" selected>Shipper</option>
      <option value="DRIVER" class="form-option">Driver</option>
    </select>
    </label>
    <button type="submit" class="form-btn">Register</button>
</form>`;
  registerForm = document.querySelector('.register-form');
  registerForm.addEventListener('submit', onRegisterFormSubmit);
}

function onRegisterFormSubmit(event) {
  onFormSubmit(event);
  if (!success) {
    return;
  }
  onLoginBtnClick();
  // success = false;
}

// -------------------------Login----------------------------

function onLoginBtnClick() {
  registerSection.innerHTML = '';
  loginSection.innerHTML = `<form action="http://localhost:8080/api/auth/login" class="login-form" method="post">
    <legend class="form-title">Login</legend>
    <label for="email" class="form-label">Email</label>
    <input id="email" type="text" name="email" class="form-input">
    <label for="password" class="form-label">Password</label>
    <input id="password" type="password" name="password" class="form-input">
    <button type="submit" class="form-btn">Login</button>
</form>
<div class="form-link-wrapper"><a class="form-link">Forgot password</a></div>`;
  loginForm = document.querySelector('.login-form');
  loginForm.addEventListener('submit', onLoginFormSubmit);
  forgotPasswordLink = document.querySelector('.form-link');
  forgotPasswordLink.addEventListener('click', onForgotPasswordLinkClick);
  forgotPasswordLinkWrapper = document.querySelector('.form-link-wrapper');
}

async function onLoginFormSubmit(event) {
  onFormSubmit(event);
  if (!success) {
    return;
  }
  if (token) {
    await getUser();
  }
  loginSection.innerHTML = '';
  login.hidden = true;
  register.hidden = true;
  logout.hidden = false;
  loads.hidden = false;
  profile.hidden = false;
  trucks.hidden = false;
  success = false;
}

// -----------------------Forgot password------------------------

function onForgotPasswordLinkClick() {
  loginForm.innerHTML = '';
  const emailForm = `<form action="http://localhost:8080/api/auth/forgot_password" class="login-form send">
        <label for="email" class="form-label">Please enter your email:</label>
        <input type="email" name="email" class="form-input" />
      <button type="submit" class="form-btn send">Send new password</button>
    </form>`;
  forgotPasswordLinkWrapper.innerHTML = emailForm;
  const sendEmailForm = document.querySelector('.send');
  sendEmailForm.addEventListener('submit', onSendEmailSubmit);
  content.innerHTML = '';
}

function onSendEmailSubmit(event) {
  try {
    success = false;
    onFormSubmit(event);
    if (success) {
      forgotPasswordLinkWrapper.innerHTML = '<p>New password has been sent to your email</p>';
    }
  } catch (error) {
    alert(error);
  }
}

// -----------------------Logout---------------------------

function onLogoutBtnClick() {
  localStorage.removeItem('token');
  localStorage.removeItem('role');
  loadsSection.innerHTML = '';
  profileSection.innerHTML = '';
  trucksSection.innerHTML = '';
  loads.hidden = true;
  logout.hidden = true;
  trucks.hidden = true;
  profile.hidden = true;
  register.hidden = false;
  login.hidden = false;
  home.classList.add('current');
  main.innerHTML = '<h1 class="main-title">Delivery service app</h1>';
}

// --------------------Submit form----------------------------

async function onFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const url = form.action;
  try {
    const formData = new FormData(form);
    await postFormData({ url, formData, headers });
    form.reset();
  } catch (error) {
    alert(error);
  }
}

async function postFormData({ url, formData, headers }) {
  const dataObject = Object.fromEntries(formData.entries());
  const formDataString = JSON.stringify(dataObject);

  const fetchOptions = {
    method: 'POST',
    headers,
    body: formDataString,
  };
  const response = await fetch(url, fetchOptions);

  if (!response.ok) {
    const errorMessage = await response.json();
    success = false;
    return alert(errorMessage.message);
  }

  const result = await response.json();

  if (result.jwt_token) {
    localStorage.setItem('token', result.jwt_token);
  }

  return result;
}

// ---------------------------------------------------------------------
// ------------------------------Loads----------------------------------
// ---------------------------------------------------------------------

loads.addEventListener('click', onLoadsPageClick);

function onLoadsPageClick() {
  main.innerHTML = '';
  trucksSection.innerHTML = '';
  profileSection.innerHTML = '';
  if (userRole === 'DRIVER') {
    loadsSection.innerHTML = `<div class="loads-menu">
    <ul class="loads-menu__list">
      <li class="loads-menu__item active current-point">ACTIVE LOAD</li>
      <li class="loads-menu__item history">HISTORY</li>
    </ul>
  </div>
  <div class="loads-menu__details">
    <ul class="loads-menu__details-list">
      <li class="loads-menu__details-item">LOAD NAME</li>
      <li class="loads-menu__details-item">CREATED DATE</li>
      <li class="loads-menu__details-item">PICK-UP ADDRESS</li>
      <li class="loads-menu__details-item">DELIVERY ADDRESS</li>
      <li class="loads-menu__details-item"></li>
    </ul>
    <ul class="loads-list"></ul>
  </div>`;
    history = document.querySelector('.history');
    activeLoadLink = document.querySelector('.active');
    activeLoadLink.addEventListener('click', renderActiveLoad);
    renderActiveLoad();
  } else {
    loadsSection.innerHTML = `<div class="loads-menu">
    <ul class="loads-menu__list">
    <li class="loads-menu__item new current-point">NEW LOADS</li>
    <li class="loads-menu__item assigned">ASSIGNED LOADS</li>
    <li class="loads-menu__item history">HISTORY</li>
    <li class="loads-menu__item add">ADD LOADS</li>
    </ul>
  </div>
  <div class="loads-menu__details">
    <ul class="loads-menu__details-list">
      <li class="loads-menu__details-item">LOAD NAME</li>
      <li class="loads-menu__details-item">CREATED DATE</li>
      <li class="loads-menu__details-item">PICK-UP ADDRESS</li>
      <li class="loads-menu__details-item">DELIVERY ADDRESS</li>
      <li class="loads-menu__details-item"></li>
    </ul>
    <ul class="loads-list"></ul>
  </div>`;
    loadsList = document.querySelector('.loads-list');
    newLoads = document.querySelector('.new');
    assignedLoads = document.querySelector('.assigned');
    addLoad = document.querySelector('.add');
    newLoads.addEventListener('click', getNewLoads);
    assignedLoads.addEventListener('click', getAssignedLoads);
    addLoad.addEventListener('click', addNewLoadForm);
    history = document.querySelector('.history');
    getNewLoads();
  }
  history.addEventListener('click', getHistory);
  loads.classList.add('current');
  home.classList.remove('current');
  trucks.classList.remove('current');
  profile.classList.remove('current');
}

function getNewLoads() {
  assignedLoads.classList.remove('current-point');
  history.classList.remove('current-point');
  newLoads.classList.add('current-point');
  getLoadsByStatus('NEW');
}

function getAssignedLoads() {
  assignedLoads.classList.add('current-point');
  history.classList.remove('current-point');
  newLoads.classList.remove('current-point');
  getLoadsByStatus('ASSIGNED');
}

async function getLoadsByStatus(status, offset = 0, limit = 10) {
  let url;
  if (!status) {
    url = `http://localhost:8080/api/loads/?offset=${offset}&limit=${limit}`;
  } else {
    url = `http://localhost:8080/api/loads/?status=${status}&offset=${offset}&limit=${limit}`;
  }

  const fetchOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };

  const response = await fetch(url, fetchOptions);

  if (response.ok) {
    const result = await response.json();
    if (result.loads.length === 0) {
      alert('Your load list is empty');
    }
    renderLoads(result.loads);
  }
}

async function getHistory() {
  if (assignedLoads) {
    assignedLoads.classList.remove('current-point');
    newLoads.classList.remove('current-point');
  }
  if (activeLoadLink) {
    activeLoadLink.classList.remove('current-point');
  }
  history.classList.add('current-point');
  getLoadsByStatus('SHIPPED');
}

function loadListTemplate(data) {
  return data
    .map(
      ({
        _id,
        name,
        created_date,
        pickup_address,
        delivery_address,
      }) => `<li  class="loads-item">
      <ul class="loads-menu__details-list">
      <li class="loads-menu__details-item">${name}</li>
      <li class="loads-menu__details-item">${created_date.split('T')[0]}</li>
      <li class="loads-menu__details-item">${pickup_address}</li>
      <li class="loads-menu__details-item">${delivery_address}</li>
      <li class="loads-menu__details-item see">
        <ul class="tools">
          <li id="edit${_id}" class="edit">
            <svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="grey">
              <path d="M27 0c2.761 0 5 2.239 5 5 0 1.126-0.372 2.164-1 3l-2 2-7-7 2-2c0.836-0.628 1.874-1 3-1zM2 23l-2 9 9-2 18.5-18.5-7-7-18.5 18.5zM22.362 11.362l-14 14-1.724-1.724 14-14 1.724 1.724z"></path>
            </svg>
          </li>
          <li id="post${_id}" class="post">
            <svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="grey">
              <path d="M32 18l-4-8h-6v-4c0-1.1-0.9-2-2-2h-18c-1.1 0-2 0.9-2 2v16l2 2h2.536c-0.341 0.588-0.536 1.271-0.536 2 0 2.209 1.791 4 4 4s4-1.791 4-4c0-0.729-0.196-1.412-0.536-2h11.073c-0.341 0.588-0.537 1.271-0.537 2 0 2.209 1.791 4 4 4s4-1.791 4-4c0-0.729-0.196-1.412-0.537-2h2.537v-6zM22 18v-6h4.146l3 6h-7.146z"></path>
            </svg>
          </li>
          <li id="delete${_id}" class="delete">
            <svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="grey">
              <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
              <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
            </svg>
          </li>
        </ul>
      </li>
      </ul>
      </li>`,
    )
    .join('');
}

function renderLoads(data) {
  if (activeLoadLink) {
    activeLoadLink.classList.add('current-point');
  }
  history.classList.remove('current-point');
  loadsList.innerHTML = loadListTemplate(data);
  postBtns = document.querySelectorAll('.post');
  postBtns.forEach((btn) => btn.addEventListener('click', onPostBtnClick));
  deleteBtns = document.querySelectorAll('.delete');
  deleteBtns.forEach((btn) => btn.addEventListener('click', onDeleteBtnClick));
  editBtns = document.querySelectorAll('.edit');
  editBtns.forEach((btn) => btn.addEventListener('click', onEditBtnClick));
}

async function onEditBtnClick(event) {
  event.preventDefault();
  const id = event.currentTarget.id.slice(4);
  method = 'PUT';
  url = `http://localhost:8080/api/loads/${id}`;
  await editLoad();
  await getLoadsByStatus('NEW');
}

function editLoad() {
  backdrop.classList.toggle('is-hidden');
  closeBtn.addEventListener('click', onCloseBtnClick);
  loadForm.addEventListener('submit', onLoadFormSubmit);
}

async function onPostBtnClick(event) {
  event.preventDefault();
  const id = event.currentTarget.id.slice(4);
  await postLoad(id);
  await getLoadsByStatus('NEW');
}

async function postLoad(loadId) {
  return fetch(`http://localhost:8080/api/loads/${loadId}/post`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        alert('Load is posted');
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert('There are no free trucks'));
}

async function onDeleteBtnClick(event) {
  event.preventDefault();
  const id = event.currentTarget.id.slice(6);
  deleteLoad(id);
  await getLoadsByStatus('NEW');
}

async function deleteLoad(loadId) {
  return fetch(`http://localhost:8080/api/loads/${loadId}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert('Load not found'));
}

function addNewLoadForm() {
  method = 'POST';
  url = 'http://localhost:8080/api/loads/';
  backdrop.classList.toggle('is-hidden');
  closeBtn.addEventListener('click', onCloseBtnClick);
  loadForm.addEventListener('submit', onLoadFormSubmit);
}

function onCloseBtnClick() {
  backdrop.classList.toggle('is-hidden');
}

function onLoadFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  loadData = Object.fromEntries(formData.entries());
  backdrop.classList.toggle('is-hidden');
  backdropDimensions.classList.toggle('is-hidden');
  const formDimensions = document.querySelector('.form-dimensions');
  formDimensions.addEventListener('submit', onLoadInformationSubmit);
  form.reset();
}

async function onLoadInformationSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  loadData.dimensions = Object.fromEntries(formData.entries());
  backdropDimensions.classList.toggle('is-hidden');
  await saveLoad();
  form.reset();
  await getNewLoads();
}

function saveLoad() {
  return fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(loadData),
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert('Bad data'));
}

async function getActiveLoads() {
  return fetch('http://localhost:8080/api/loads/active', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json().then((data) => data.load);
      }
      throw new Error();
    })
    .catch(() => alert('You have\'t any active load'));
}

async function renderActiveLoad() {
  const activeLoad = await getActiveLoads();
  loadsList = document.querySelector('.loads-list');
  if (activeLoad) {
    loadsList.innerHTML = activeLoadTemplate(activeLoad);
    const state = document.querySelector('.state');
    state.addEventListener('click', changeState);
  } else {
    loadsList.innerHTML = '<p class="message">You have\'t any active load</p>';
  }
}

function activeLoadTemplate({
  _id, name, created_date, pickup_address, delivery_address, state,
}) {
  return `<li  class="loads-item">
      <ul class="loads-menu__details-list">
      <li class="loads-menu__details-item">${name}</li>
      <li class="loads-menu__details-item">${created_date.split('T')[0]}</li>
      <li class="loads-menu__details-item">${pickup_address}</li>
      <li class="loads-menu__details-item">${delivery_address}</li>
      <li class="loads-menu__details-item state"><span>${state}</span>
        <svg id="${_id}" class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
          <path d="M27.802 5.197c-2.925-3.194-7.13-5.197-11.803-5.197-8.837 0-16 7.163-16 16h3c0-7.18 5.82-13 13-13 3.844 0 7.298 1.669 9.678 4.322l-4.678 4.678h11v-11l-4.198 4.197z"></path>
          <path d="M29 16c0 7.18-5.82 13-13 13-3.844 0-7.298-1.669-9.678-4.322l4.678-4.678h-11v11l4.197-4.197c2.925 3.194 7.13 5.197 11.803 5.197 8.837 0 16-7.163 16-16h-3z"></path>
        </svg>
      </li>
      </ul>
      </li>`;
}

function changeState() {
  return fetch('http://localhost:8080/api/loads/active/state', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json().then(() => renderActiveLoad());
      }
      throw new Error();
    })
    .catch(() => alert('You haven`t active loads'));
}

// ------------------------------Profile----------------------------------

profile.addEventListener('click', onProfilePageClick);

function onProfilePageClick() {
  loadsSection.innerHTML = '';
  trucksSection.innerHTML = '';
  main.innerHTML = '';
  profile.classList.add('current');
  loads.classList.remove('current');
  trucks.classList.remove('current');
  home.classList.remove('current');
  getCurrentUserInfo();
}

async function getCurrentUserInfo() {
  await getUser();
  profileSection.innerHTML = `<div class="user-info"><h3>User information</h3>
  <p>Email: ${userInfo.email}</p>
  <p>Role: ${userInfo.role}</p>
  <p>Created date: ${userInfo.created_date.split('T')[0]}</p></div>`;
}

function getUser() {
  return fetch('http://localhost:8080/api/users/me', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json().then((data) => {
          userInfo = data.user;
          localStorage.setItem('role', userInfo.role);
          userRole = localStorage.getItem('role');
          if (userRole === 'SHIPPER') {
            trucks.hidden = true;
          }
          return data;
        });
      }
      throw new Error();
    })
    .catch(() => alert('Not authorized'));
}

// ------------------------------Trucks----------------------------------

trucks.addEventListener('click', onTrucksPageClick);

let allTrucks;
let addTruck;

function onTrucksPageClick() {
  loadsSection.innerHTML = '';
  profileSection.innerHTML = '';
  main.innerHTML = '';
  profile.classList.remove('current');
  loads.classList.remove('current');
  trucks.classList.add('current');
  home.classList.remove('current');
  trucksSection.innerHTML = `<div class="trucks-menu">
  <ul class="trucks-menu__list">
  <li class="trucks-menu__item all current-point">All TRUCKS</li>
  <li class="trucks-menu__item add-truck">ADD TRUCK</li>
  </ul>
</div>
<div class="trucks-menu__details">
  <ul class="trucks-menu__details-list">
    <li class="trucks-menu__details-item">TYPE</li>
    <li class="trucks-menu__details-item">CREATED DATE</li>
    <li class="trucks-menu__details-item">STATUS</li>
    <li class="trucks-menu__details-item"></li>
  </ul>
  <ul class="trucks-list"></ul>
</div>`;
  allTrucks = document.querySelector('.all');
  allTrucks.addEventListener('click', getAllTrucks);
  addTruck = document.querySelector('.add-truck');
  addTruck.addEventListener('click', onAddTruckBtnClick);
  getAllTrucks();
}

function getAllTrucks() {
  addTruck.classList.remove('current-point');
  allTrucks.classList.add('current-point');
  fetchTrucks();
}

async function fetchTrucks() {
  const fetchOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };

  const response = await fetch('http://localhost:8080/api/trucks', fetchOptions);

  if (response.ok) {
    const result = await response.json();
    if (result.trucks.length === 0) {
      alert('Your truck list is empty');
    }
    renderTrucks(result.trucks);
  }
}

function truckListTemplate(data) {
  return data
    .map(
      ({
        _id,
        type,
        created_date,
        status,
        assigned_to,
      }) => `<li  class="trucks-item">
      <ul class="trucks-menu__details-list">
      <li class="trucks-menu__details-item">${type}</li>
      <li class="trucks-menu__details-item">${created_date.split('T')[0]}</li>
      <li class="trucks-menu__details-item green ${assigned_to}">${status}</li>
      <li class="trucks-menu__details-item">
        <ul class="tools">
          <li id="edit${_id}" class="edit">
            <svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="grey">
              <path d="M27 0c2.761 0 5 2.239 5 5 0 1.126-0.372 2.164-1 3l-2 2-7-7 2-2c0.836-0.628 1.874-1 3-1zM2 23l-2 9 9-2 18.5-18.5-7-7-18.5 18.5zM22.362 11.362l-14 14-1.724-1.724 14-14 1.724 1.724z"></path>
            </svg>
          </li>
          <li id="assign${_id}" class="assign">
            <svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="grey">
              <path d="M32 18l-4-8h-6v-4c0-1.1-0.9-2-2-2h-18c-1.1 0-2 0.9-2 2v16l2 2h2.536c-0.341 0.588-0.536 1.271-0.536 2 0 2.209 1.791 4 4 4s4-1.791 4-4c0-0.729-0.196-1.412-0.536-2h11.073c-0.341 0.588-0.537 1.271-0.537 2 0 2.209 1.791 4 4 4s4-1.791 4-4c0-0.729-0.196-1.412-0.537-2h2.537v-6zM22 18v-6h4.146l3 6h-7.146z"></path>
            </svg>
          </li>
          <li id="delete${_id}" class="delete">
            <svg class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="grey">
              <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
              <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
            </svg>
          </li>
        </ul>
      </li>
      </ul>
      </li>`,
    ).join('');
}

function renderTrucks(data) {
  truckList = document.querySelector('.trucks-list');
  truckList.innerHTML = truckListTemplate(data);
  assignBtns = document.querySelectorAll('.assign');
  assignBtns.forEach((btn) => btn.addEventListener('click', onAssignBtnClick));
  deleteBtns = document.querySelectorAll('.delete');
  deleteBtns.forEach((btn) => btn.addEventListener('click', onDeleteTruckBtnClick));
  editBtns = document.querySelectorAll('.edit');
  editBtns.forEach((btn) => btn.addEventListener('click', onEditTruckBtnClick));
}

async function onEditTruckBtnClick(event) {
  event.preventDefault();
  const id = event.currentTarget.id.slice(4);
  method = 'PUT';
  url = `http://localhost:8080/api/trucks/${id}`;
  await editTruck();
  await getAllTrucks();
}

function editTruck() {
  backdropTruck.classList.toggle('is-hidden');
  closeTruckBtn.addEventListener('click', onCloseTruckBtnClick);
  loadForm.addEventListener('submit', onTruckFormSubmit);
}

function onCloseTruckBtnClick() {
  backdropTruck.classList.toggle('is-hidden');
}

async function onDeleteTruckBtnClick(event) {
  event.preventDefault();
  const id = event.currentTarget.id.slice(6);
  await deleteTruck(id);
  await getAllTrucks();
}

async function deleteTruck(truckId) {
  return fetch(`http://localhost:8080/api/trucks/${truckId}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert('Truck cannot be deleted'));
}

function onAddTruckBtnClick() {
  backdropTruck.classList.toggle('is-hidden');
  closeTruckBtn.addEventListener('click', onCloseTruckBtnClick);
  loadForm.addEventListener('submit', onTruckFormSubmit);
  addNewTruck();
}

function addNewTruck() {
  method = 'POST';
  url = 'http://localhost:8080/api/trucks/';
  truckForm.addEventListener('submit', onTruckFormSubmit);
}

async function onTruckFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  const truckData = Object.fromEntries(formData.entries());
  await saveTruck(truckData);
  backdropTruck.classList.toggle('is-hidden');
  form.reset();
  await getAllTrucks();
}

function saveTruck(truckData) {
  return fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(truckData),
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert('Bad data'));
}

async function onAssignBtnClick(event) {
  event.preventDefault();
  const id = event.currentTarget.id.slice(6);
  console.log(id);
  await assignTruckPost(id);
}

function assignTruckPost(id) {
  return fetch(`http://localhost:8080/api/trucks/${id}/assign`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json().then(() => getAllTrucks());
      }
      throw new Error();
    })
    .catch(() => alert('You cann`t assign more then one truck'));
}
