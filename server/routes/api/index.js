const trucksRouter = require('./trucks');
const usersRouter = require('./users');
const authRouter = require('./auth');
const loadsRouter = require('./loads');

module.exports = {
  trucksRouter,
  usersRouter,
  authRouter,
  loadsRouter,
};
