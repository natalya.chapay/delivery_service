const { Load } = require('../../models/load');

const loadList = async (req, res) => {
  const { _id } = req.user;
  const { offset = 0, limit = 10, status } = req.query;
  console.log(JSON.stringify(status));
  let query = { $or: [{ created_by: _id }, { assigned_to: _id }] };
  if (status) {
    query = { $or: [{ created_by: _id }, { assigned_to: _id }], status };
  }
  const loads = await Load.find(query, '', { skip: Number(offset), limit: Number(limit) });

  res.json({
    loads,
  });
};

module.exports = loadList;
