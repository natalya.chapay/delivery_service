const { createError } = require('../../helpers');
const { Load } = require('../../models/load');

const addLoad = async (req, res) => {
  const { _id } = req.user;
  const { id } = req.params;
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  const load = await Load.findOne({ id, status: 'NEW' });
  if (!load) {
    throw createError(400);
  }
  await load.updateOne({
    name, payload, pickup_address, delivery_address, dimensions, created_by: _id,
  });
  res.status(200).json({
    message: 'Load details changed successfully',
  });
};

module.exports = addLoad;
