const { createError } = require('../../helpers');

const { Load } = require('../../models/load');

const getActive = async (req, res) => {
  const { _id } = req.user;
  console.log(_id);
  const load = await Load.findOne({ assigned_to: _id, status: 'ASSIGNED' });
  if (!load) {
    throw createError(400);
  }
  res.json({
    load,
  });
};

module.exports = getActive;
