const { createError } = require('../../helpers');

const { Load } = require('../../models/load');

const removeNote = async (req, res) => {
  const { id } = req.params;
  const load = await Load.findOne({ id, status: 'NEW' });
  if (!load) {
    throw createError(400);
  }
  await load.deleteOne({ id });

  res.status(200).json({
    message: 'Load deleted successfully',
  });
};

module.exports = removeNote;
