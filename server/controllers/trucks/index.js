const addTruck = require('./addTruck');
const deleteTruck = require('./deleteTruck');
const getById = require('./getById');
const truckList = require('./truckList');
const updateTruck = require('./updateTruck');
const assignTruck = require('./assignTruck');

module.exports = {
  addTruck,
  deleteTruck,
  getById,
  truckList,
  updateTruck,
  assignTruck,
};
