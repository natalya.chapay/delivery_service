const { Schema, model } = require('mongoose');

const userSchema = Schema({
  email: {
    type: String,
    required: [true, 'Email is required'],
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
  },
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
  avatarURL: {
    type: String,
    required: true,
  },
}, { versionKey: false, timestamps: { createdAt: 'created_date', updatedAt: false } });

const User = model('user', userSchema);

module.exports = {
  User,
};
